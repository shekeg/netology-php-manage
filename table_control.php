<?php
  require_once(__DIR__ . '/db.php');
  require_once(__DIR__ . '/functions.php');

  if (!empty($_POST['save'])) {
    saveFieldChanges($pdo, $_POST['table'], $_POST['field_old'], $_POST['field_new'], $_POST['type']);
  }

  if (!empty($_GET['action']) and $_GET['action'] === 'delete') {
    deleteField($pdo, $_GET['table'], $_GET['field']);
  }

  if (!empty($_GET['table'])) $tableName = $_GET['table'];
?>

<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Информация о таблице</title>
</head>
  <h1><?php echo $tableName ?></h1>
<body>
  <?php if (!empty($_GET['action']) and $_GET['action'] === 'edit') : ?>
    <form action="table_control.php" method="POST">
      <input type="text" name="table" id="table" value="<?php echo $_GET['table'] ?>" hidden>
      <input type="text" name="field_old" id="field-old" value="<?php echo $_GET['field'] ?>" hidden>
      <input type="text" name="field_new" id="field-new" value="<?php echo $_GET['field'] ?>">
      <input type="text" name="type" id="field-type" value="<?php echo $_GET['type'] ?>">
      <input type="submit" name="save" value="Сохранить">
    </form>
  <?php endif; ?>
  <table>
    <thead>
      <th>Field</th>
      <th>Type</th>
      <th>Null</th>
      <th>Key</th>
      <th>Default</th>
      <th>Extra</th>
      <th>Настройка</th>
    </thead>
    <tbody>
      <?php foreach(getTableInfo($pdo, $tableName) as $field) : ?>
        <tr>
          <td><?php echo $field['Field'] ?></td>
          <td><?php echo $field['Type'] ?></td>
          <td><?php echo $field['Null'] ?></td>
          <td><?php echo $field['Key'] ?></td>
          <td><?php echo $field['Default'] ?></td>
          <td><?php echo $field['Extra'] ?></td>
          <td>
            <a href="table_control.php?action=edit&table=<?php echo $tableName ?>&field=<?php echo $field['Field'] ?>&type=<?php echo $field['Type'] ?>">Изменить</a>
            <a href="table_control.php?action=delete&table=<?php echo $tableName ?>&field=<?php echo $field['Field'] ?>">Удалить</a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</body>
</html>