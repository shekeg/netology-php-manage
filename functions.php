<?php
  function createStandartTable($pdo, $tableName) {
    $sql = "CREATE TABLE IF NOT EXISTS `$tableName` (
      id int NOT NULL AUTO_INCREMENT,
      name varchar(50) NULL,
      estimation float NOT NULL,
      budget tinyint(4) NOT NULL DEFAULT '0',
      PRIMARY KEY (id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
    $pdo->query($sql);
  }

  function getTables($pdo) {
    $sql = "SHOW TABLES";
    $tables = $pdo->query($sql);
    return $tables;
  }

  function getTableInfo($pdo, $tableName) {
    $sql = "DESCRIBE $tableName";
    $fields = $pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    return $fields;
  }
 
  function saveFieldChanges($pdo, $tableName, $fieldOld, $fieldNew, $type) {
    $sql = "ALTER TABLE $tableName CHANGE $fieldOld  $fieldNew $type;";
    $pdo->query($sql);
    header("Location: table_control.php?table=$tableName");
  }
  
  function deleteField($pdo, $tableName, $field) {
    $sql = "ALTER TABLE $tableName DROP $field";
    $pdo->query($sql);
  }